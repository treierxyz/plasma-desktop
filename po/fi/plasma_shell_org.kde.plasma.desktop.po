# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Lasse Liehu <lasse.liehu@gmail.com>, 2013, 2014, 2015, 2016, 2017, 2022.
# Tommi Nieminen <translator@legisign.org>, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
#
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2023-05-28 16:55+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.04.1\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Käytössä"

#: contents/activitymanager/ActivityItem.qml:245
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Siirry tähän\n"
"aktiviteettiin"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Näytä myös\n"
"tässä aktiviteetissa"

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Asetukset"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Pysäytä aktiviteetti"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Pysäytetyt aktiviteetit:"

#: contents/activitymanager/ActivityManager.qml:122
msgid "Create activity…"
msgstr "Luo aktiviteetti…"

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Aktiviteetit"

#: contents/activitymanager/StoppedActivityItem.qml:138
msgid "Configure activity"
msgstr "Muokkaa aktiviteetin asetuksia"

#: contents/activitymanager/StoppedActivityItem.qml:155
msgid "Delete"
msgstr "Poista"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr "Ladattaessa tapahtui valitettavasti virhe: %1."

#: contents/applet/AppletError.qml:167
msgid "Copy to Clipboard"
msgstr "Kopioi leikepöydälle"

#: contents/applet/AppletError.qml:190
msgid "View Error Details…"
msgstr "Näytä virheen yksityiskohdat…"

#: contents/applet/CompactApplet.qml:76
msgid "Open %1"
msgstr "Avaa %1"

#: contents/configuration/AboutPlugin.qml:21
#: contents/configuration/AppletConfiguration.qml:304
msgid "About"
msgstr "Tietoa"

#: contents/configuration/AboutPlugin.qml:50
msgid "Send an email to %1"
msgstr "Lähetä sähköpostia: %1"

#: contents/configuration/AboutPlugin.qml:64
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Avaa sivusto %1"

#: contents/configuration/AboutPlugin.qml:137
msgid "Copyright"
msgstr "Tekijänoikeudet"

#: contents/configuration/AboutPlugin.qml:158 contents/explorer/Tooltip.qml:94
msgid "License:"
msgstr "Lisenssi:"

#: contents/configuration/AboutPlugin.qml:163
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "Näytä lisenssin teksti"

#: contents/configuration/AboutPlugin.qml:177
msgid "Authors"
msgstr "Tekijät"

#: contents/configuration/AboutPlugin.qml:189
msgid "Credits"
msgstr "Kiitokset"

#: contents/configuration/AboutPlugin.qml:202
msgid "Translators"
msgstr "Kääntäjät"

#: contents/configuration/AboutPlugin.qml:220
msgid "Report a Bug…"
msgstr "Ilmoita viasta…"

#: contents/configuration/AppletConfiguration.qml:76
msgid "Keyboard Shortcuts"
msgstr "Pikanäppäimet"

#: contents/configuration/AppletConfiguration.qml:352
msgid "Apply Settings"
msgstr "Käytä asetuksia"

#: contents/configuration/AppletConfiguration.qml:353
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Nykyisen asetusosion asetukset ovat muuttuneet. Haluatko käyttää muutoksia "
"vai hylätä ne?"

#: contents/configuration/AppletConfiguration.qml:384
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:392
msgid "Apply"
msgstr "Käytä"

#: contents/configuration/AppletConfiguration.qml:398
msgid "Cancel"
msgstr "Peru"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "Avaa asetussivu"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Vasen painike"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Oikea painike"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Keskipainike"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Takaisin-painike"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Eteenpäin-painike"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Pystyvieritys"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Vaakavieritys"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Vaihto"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr "+"

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "Tietoa"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Lisää toiminto"

#: contents/configuration/ConfigurationContainmentAppearance.qml:69
msgid "Layout changes have been restricted by the system administrator"
msgstr "Järjestelmänvalvoja on rajoittanut asettelumuutoksia"

#: contents/configuration/ConfigurationContainmentAppearance.qml:84
msgid "Layout:"
msgstr "Asettelu:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:98
msgid "Wallpaper type:"
msgstr "Taustan tyyppi:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:119
msgid "Get New Plugins…"
msgstr "Hae uusia liitännäisiä…"

#: contents/configuration/ConfigurationContainmentAppearance.qml:189
msgid "Layout changes must be applied before other changes can be made"
msgstr "Asettelumuutoksia täytyy käyttää ennen muiden muutosten tekemistä"

#: contents/configuration/ConfigurationContainmentAppearance.qml:193
msgid "Apply Now"
msgstr "Käytä nyt"

#: contents/configuration/ConfigurationShortcuts.qml:18
msgid "Shortcuts"
msgstr "Pikanäppäimet"

#: contents/configuration/ConfigurationShortcuts.qml:30
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "Tämä pikanäppäin aktivoi sovelman kuin sitä olisi napsautettu."

#: contents/configuration/ContainmentConfiguration.qml:27
msgid "Wallpaper"
msgstr "Tausta"

#: contents/configuration/ContainmentConfiguration.qml:32
msgid "Mouse Actions"
msgstr "Hiiren toiminnot"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Syötä tähän"

#: contents/configuration/PanelConfiguration.qml:91
msgid "Panel Settings"
msgstr "Paneeliasetukset"

#: contents/configuration/PanelConfiguration.qml:98
msgid "Add Spacer"
msgstr "Lisää erotin"

#: contents/configuration/PanelConfiguration.qml:101
msgid "Add spacer widget to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:108
msgid "Add Widgets…"
msgstr "Lisää sovelmia…"

#: contents/configuration/PanelConfiguration.qml:111
msgid "Open the widget selector to drag and drop widgets to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:140
msgid "Position"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:145
#: contents/configuration/PanelConfiguration.qml:260
msgid "Top"
msgstr "Ylhäällä"

#: contents/configuration/PanelConfiguration.qml:146
#: contents/configuration/PanelConfiguration.qml:262
msgid "Right"
msgstr "Oikealla"

#: contents/configuration/PanelConfiguration.qml:147
#: contents/configuration/PanelConfiguration.qml:260
msgid "Left"
msgstr "Vasemmalla"

#: contents/configuration/PanelConfiguration.qml:148
#: contents/configuration/PanelConfiguration.qml:262
msgid "Bottom"
msgstr "Alhaalla"

#: contents/configuration/PanelConfiguration.qml:168
#, fuzzy
#| msgid "More Options…"
msgid "Set Position…"
msgstr "Lisää valintoja…"

#: contents/configuration/PanelConfiguration.qml:195
#, fuzzy
#| msgid "Alignment:"
msgid "Alignment"
msgstr "Tasaus:"

#: contents/configuration/PanelConfiguration.qml:261
msgid "Center"
msgstr "Keskellä"

#: contents/configuration/PanelConfiguration.qml:284
msgid "Length"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:303
#, fuzzy
#| msgid "Panel height:"
msgid "Fill height"
msgstr "Paneelin korkeus:"

#: contents/configuration/PanelConfiguration.qml:303
#, fuzzy
#| msgid "Panel width:"
msgid "Fill width"
msgstr "Paneelin leveys:"

#: contents/configuration/PanelConfiguration.qml:304
msgid "Fit content"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:305
msgid "Custom"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:335
#, fuzzy
#| msgid "Visibility:"
msgid "Visibility"
msgstr "Näkyvyys:"

#: contents/configuration/PanelConfiguration.qml:348
#, fuzzy
#| msgid "Always Visible"
msgid "Always visible"
msgstr "Aina näkyvissä"

#: contents/configuration/PanelConfiguration.qml:349
#, fuzzy
#| msgid "Auto-Hide"
msgid "Auto hide"
msgstr "Piilota automaattisesti"

#: contents/configuration/PanelConfiguration.qml:350
msgid "Dodge windows"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:351
#, fuzzy
#| msgid "Windows Go Below"
msgid "Windows Go Below"
msgstr "Ikkunat jäävät alle"

#: contents/configuration/PanelConfiguration.qml:394
#, fuzzy
#| msgid "Opacity:"
msgid "Opacity"
msgstr "Peittävyys:"

#: contents/configuration/PanelConfiguration.qml:408
msgid "Adaptive"
msgstr "Mukautuva"

#: contents/configuration/PanelConfiguration.qml:409
#, fuzzy
#| msgid "Always Opaque"
msgid "Opaque"
msgstr "Aina peittävä"

#: contents/configuration/PanelConfiguration.qml:410
#, fuzzy
#| msgid "Always Translucent"
msgid "Translucent"
msgstr "Aina läpikuultava"

#: contents/configuration/PanelConfiguration.qml:434
msgid "Style"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:446
msgid "Floating"
msgstr "Kelluva"

#: contents/configuration/PanelConfiguration.qml:520
#, fuzzy
#| msgid "Panel width:"
msgid "Panel Width:"
msgstr "Paneelin leveys:"

#: contents/configuration/PanelConfiguration.qml:520
#, fuzzy
#| msgid "Height"
msgid "Height:"
msgstr "Korkeus"

#: contents/configuration/PanelConfiguration.qml:555
#, fuzzy
#| msgid "Focus Shortcut:"
msgid "Focus shortcut:"
msgstr "Kohdistuspikanäppäin:"

#: contents/configuration/PanelConfiguration.qml:566
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "Siirrä kohdistus paneeliin tällä pikanäppäimellä"

#: contents/configuration/PanelConfiguration.qml:582
#, fuzzy
#| msgid "Remove Panel"
msgctxt "@action:button Delete the panel"
msgid "Delete Panel"
msgstr "Poista paneeli"

#: contents/configuration/PanelConfiguration.qml:585
msgid "Remove this panel; this action is undo-able"
msgstr "Poista tämä paneeli – toimintoa ei voi kumota"

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum height."
msgstr "Muuta enimmäiskorkeutta vetämällä."

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum width."
msgstr "Muuta enimmäisleveyttä vetämällä."

#: contents/configuration/panelconfiguration/Ruler.qml:30
#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Double click to reset."
msgstr "Palauta kaksoisnapsauttamalla."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum height."
msgstr "Muuta vähimmäiskorkeutta vetämällä."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum width."
msgstr "Muuta vähimmäisleveyttä vetämällä."

#: contents/configuration/panelconfiguration/Ruler.qml:78
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Muuta tämän näytön reunan sijaintia vetämällä. Palauta kaksoisnapsauttamalla."

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Paneeli- ja työpöytähallinta"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr "Paneelit ja työpöydät voi vetämällä siirtää eri näytöille."

#: contents/configuration/ShellContainmentConfiguration.qml:44
msgid "Close"
msgstr "Sulje"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Swap with Desktop on Screen %1"
msgstr "Vaihda työpöydälle näytöllä %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:180
msgid "Move to Screen %1"
msgstr "Siirrä näytölle %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Desktop"
msgstr "Poista työpöytä"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:195
msgid "Remove Panel"
msgstr "Poista paneeli"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:268
msgid "%1 (primary)"
msgstr "%1 (ensisijainen)"

#: contents/explorer/AppletAlternatives.qml:66
msgid "Alternative Widgets"
msgstr "Vaihtoehtoiset sovelmat"

#: contents/explorer/AppletDelegate.qml:41
msgid "Unsupported Widget"
msgstr ""

#: contents/explorer/AppletDelegate.qml:188
msgid "Undo uninstall"
msgstr "Kumoa asennuksen poisto"

#: contents/explorer/AppletDelegate.qml:189
msgid "Uninstall widget"
msgstr "Poista sovelman asennus"

#: contents/explorer/Tooltip.qml:105
msgid "Author:"
msgstr "Tekijä:"

#: contents/explorer/Tooltip.qml:115
msgid "Email:"
msgstr "Sähköposti:"

#: contents/explorer/Tooltip.qml:136
msgid "Uninstall"
msgstr "Poista asennus"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:193
msgid "All Widgets"
msgstr "Kaikki sovelmat"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Sovelmat"

#: contents/explorer/WidgetExplorer.qml:153
msgid "Get New Widgets…"
msgstr "Hae uusia sovelmia…"

#: contents/explorer/WidgetExplorer.qml:204
msgid "Categories"
msgstr "Luokat"

#: contents/explorer/WidgetExplorer.qml:286
msgid "No widgets matched the search terms"
msgstr "Ei hakuehtoja vastaavia sovelmia"

#: contents/explorer/WidgetExplorer.qml:286
msgid "No widgets available"
msgstr "Sovelmia ei ole saatavilla"

#~ msgctxt "@action:button Make the panel as big as it can be"
#~ msgid "Maximize"
#~ msgstr "Suurenna"

#~ msgid "Make this panel as tall as possible"
#~ msgstr "Tee tästä paneelista niin korkea kuin mahdollista"

#~ msgid "Make this panel as wide as possible"
#~ msgstr "Tee tästä paneelista niin leveä kuin mahdollista"

#~ msgctxt "@action:button Delete the panel"
#~ msgid "Delete"
#~ msgstr "Poista"

#~ msgid ""
#~ "Aligns a non-maximized panel to the top; no effect when panel is maximized"
#~ msgstr ""
#~ "Tasaa suurentamattoman paneelin ylös – ei vaikutusta, kun paneeli on "
#~ "suurennettu"

#~ msgid ""
#~ "Aligns a non-maximized panel to the left; no effect when panel is "
#~ "maximized"
#~ msgstr ""
#~ "Tasaa suurentamattoman paneelin vasemmalle – ei vaikutusta, kun paneeli "
#~ "on suurennettu"

#~ msgid ""
#~ "Aligns a non-maximized panel to the center; no effect when panel is "
#~ "maximized"
#~ msgstr ""
#~ "Keskittää suurentamattoman paneelin – ei vaikutusta, kun paneeli on "
#~ "suurennettu"

#~ msgid ""
#~ "Aligns a non-maximized panel to the bottom; no effect when panel is "
#~ "maximized"
#~ msgstr ""
#~ "Tasaa suurentamattoman paneelin alas – ei vaikutusta, kun paneeli on "
#~ "suurennettu"

#~ msgid ""
#~ "Aligns a non-maximized panel to the right; no effect when panel is "
#~ "maximized"
#~ msgstr ""
#~ "Tasaa suurentamattoman paneelin oikealle – ei vaikutusta, kun paneeli on "
#~ "suurennettu"

#~ msgid ""
#~ "Panel is hidden, but reveals itself when the cursor touches the panel's "
#~ "screen edge"
#~ msgstr ""
#~ "Paneeli on piilossa, mutta paljastaa itsensä osoittimen osuessa paneelin "
#~ "näytönreunaan"

#~ msgid ""
#~ "Panel is opaque when any windows are touching it, and translucent at "
#~ "other times"
#~ msgstr ""
#~ "Paneeli on peittävä ikkunoiden koskettaessa sitä, läpikuultava muulloin"

#~ msgid "Floating:"
#~ msgstr "Kellunta:"

#~ msgid "Panel visibly floats away from its screen edge"
#~ msgstr "Paneeli kelluu näkyvästi näytönreunastaan"

#~ msgid "Attached"
#~ msgstr "Kiinnitetty"

#~ msgid "Panel is attached to its screen edge"
#~ msgstr "Paneeli on kiinnitetty näytönreunaansa"

#~ msgctxt "Minimize the length of this string as much as possible"
#~ msgid "Drag to move"
#~ msgstr "Siirrä vetämällä"

#~ msgctxt "@info:tooltip"
#~ msgid "Use arrow keys to move the panel"
#~ msgstr "Siirrä paneelia nuolinäppäimillä"

#~ msgid "Switch"
#~ msgstr "Vaihda"

#, fuzzy
#~| msgid "Windows Go Below"
#~ msgid "Windows Above"
#~ msgstr "Ikkunat jäävät alle"

#, fuzzy
#~| msgid "Windows Can Cover"
#~ msgid "Windows In Front"
#~ msgstr "Ikkunat voivat peittää"

#~ msgid "%1 (disabled)"
#~ msgstr "%1 (ei käytössä)"

#~ msgid "Appearance"
#~ msgstr "Ulkoasu"

#~ msgid "Search…"
#~ msgstr "Etsi…"

#~ msgid "Screen Edge"
#~ msgstr "Näytön reuna"

#~ msgid "Click and drag the button to a screen edge to move the panel there."
#~ msgstr ""
#~ "Siirrä paneeli näytön reunalle napsauttamalla ja vetämällä painike sinne."

#~ msgid "Width"
#~ msgstr "Leveys"

#~ msgid "Click and drag the button to resize the panel."
#~ msgstr "Muuta paneelin kokoa napsauttamalla ja vetämällä painiketta."

#~ msgid "Layout cannot be changed while widgets are locked"
#~ msgstr "Asettelua ei voi muuttaa, kun sovelmat on lukittu"

#~ msgid "Lock Widgets"
#~ msgstr "Lukitse sovelmat"

#~ msgid ""
#~ "This shortcut will activate the applet: it will give the keyboard focus "
#~ "to it, and if the applet has a popup (such as the start menu), the popup "
#~ "will be open."
#~ msgstr ""
#~ "Tämä pikanäppäin aktivoi sovelman: Se siirtää näppäimistökohdistuksen "
#~ "sovelmaan, ja jos sovelmalla on ponnahdusvalikko (kuten "
#~ "käynnistysvalikolla), ponnahdusvalikko avautuu."

#~ msgid "Stop"
#~ msgstr "Pysäytä"

#~ msgid "Activity name:"
#~ msgstr "Aktiviteetin nimi:"

#~ msgid "Create"
#~ msgstr "Luo"

#~ msgid "Are you sure you want to delete this activity?"
#~ msgstr "Haluatko varmasti poistaa tämän aktiviteetin?"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Ok"
#~ msgstr "OK"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Apply"
#~ msgstr "Käytä"

#~ msgctxt "org.kde.plasma.desktop"
#~ msgid "Cancel"
#~ msgstr "Peru"
