# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
# SPDX-FileCopyrightText: 2023 Paolo Zamponi <feus73@gmail.com>
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2023-11-29 18:13+0100\n"
"Last-Translator: Paolo Zamponi <feus73@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 23.08.3\n"

#: axesmodel.cpp:63
#, kde-format
msgctxt "@label Axis value"
msgid "Value"
msgstr "Valore"

#: buttonmodel.cpp:74
#, kde-format
msgctxt "Status of a gamepad button"
msgid "PRESSED"
msgstr "PREMUTO"

#: buttonmodel.cpp:84
#, kde-format
msgctxt "@label Button state"
msgid "State"
msgstr "Stato"

#: devicemodel.cpp:51
#, kde-format
msgctxt "Device name and path"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: ui/main.qml:22
#, kde-format
msgid "No game controllers found"
msgstr "Nessun controller di gioco trovato"

#: ui/main.qml:54
#, kde-format
msgctxt "@label:textbox"
msgid "Device:"
msgstr "Dispositivo:"

#: ui/main.qml:81
#, kde-format
msgctxt "@label Visual representation of an axis position"
msgid "Position:"
msgstr "Posizione:"

#: ui/main.qml:100
#, kde-format
msgctxt "@label Gamepad buttons"
msgid "Buttons:"
msgstr "Pulsanti:"

#: ui/main.qml:122
#, kde-format
msgctxt "@label Gamepad axes (sticks)"
msgid "Axes:"
msgstr "Assi:"
