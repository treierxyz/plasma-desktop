# Translation of plasma_shell_org.kde.plasma.desktop to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
# Øystein Steffensen-Alværvik <oysteins.omsetting@protonmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-23 00:38+0000\n"
"PO-Revision-Date: 2023-07-12 11:31+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: contents/activitymanager/ActivityItem.qml:205
msgid "Currently being used"
msgstr "Vert brukt no"

#: contents/activitymanager/ActivityItem.qml:245
msgid ""
"Move to\n"
"this activity"
msgstr ""
"Flytt til denne\n"
"aktiviteten"

#: contents/activitymanager/ActivityItem.qml:275
msgid ""
"Show also\n"
"in this activity"
msgstr ""
"Vis òg i denne\n"
"aktiviteten"

#: contents/activitymanager/ActivityItem.qml:337
msgid "Configure"
msgstr "Set opp"

#: contents/activitymanager/ActivityItem.qml:356
msgid "Stop activity"
msgstr "Stopp aktivitet"

#: contents/activitymanager/ActivityList.qml:142
msgid "Stopped activities:"
msgstr "Stoppa aktivitetar:"

#: contents/activitymanager/ActivityManager.qml:122
msgid "Create activity…"
msgstr "Legg til aktivitet …"

#: contents/activitymanager/Heading.qml:59
msgid "Activities"
msgstr "Aktivitetar"

#: contents/activitymanager/StoppedActivityItem.qml:138
msgid "Configure activity"
msgstr "Set opp aktivitet"

#: contents/activitymanager/StoppedActivityItem.qml:155
msgid "Delete"
msgstr "Slett"

#: contents/applet/AppletError.qml:128
msgid "Sorry! There was an error loading %1."
msgstr "Det oppstod dessverre ein feil ved lasting av %1."

#: contents/applet/AppletError.qml:167
msgid "Copy to Clipboard"
msgstr "Kopier til utklippstavla"

#: contents/applet/AppletError.qml:190
msgid "View Error Details…"
msgstr "Vis feildetaljar …"

#: contents/applet/CompactApplet.qml:76
msgid "Open %1"
msgstr "Opna %1"

#: contents/configuration/AboutPlugin.qml:21
#: contents/configuration/AppletConfiguration.qml:304
msgid "About"
msgstr "Om"

#: contents/configuration/AboutPlugin.qml:50
msgid "Send an email to %1"
msgstr "Send e-post til %1"

#: contents/configuration/AboutPlugin.qml:64
msgctxt "@info:tooltip %1 url"
msgid "Open website %1"
msgstr "Opna heime­side: %1"

#: contents/configuration/AboutPlugin.qml:137
msgid "Copyright"
msgstr "Opphavsrett"

#: contents/configuration/AboutPlugin.qml:158 contents/explorer/Tooltip.qml:94
msgid "License:"
msgstr "Lisens:"

#: contents/configuration/AboutPlugin.qml:163
msgctxt "@info:whatsthis"
msgid "View license text"
msgstr "Vis lisenstekst"

#: contents/configuration/AboutPlugin.qml:177
msgid "Authors"
msgstr "Opphavspersonar"

#: contents/configuration/AboutPlugin.qml:189
msgid "Credits"
msgstr "Bidragsytarar"

#: contents/configuration/AboutPlugin.qml:202
msgid "Translators"
msgstr "Omsetjarar"

#: contents/configuration/AboutPlugin.qml:220
msgid "Report a Bug…"
msgstr "Meld frå om feil …"

#: contents/configuration/AppletConfiguration.qml:76
msgid "Keyboard Shortcuts"
msgstr "Snøggtastar"

#: contents/configuration/AppletConfiguration.qml:352
msgid "Apply Settings"
msgstr "Bruk innstillingar"

#: contents/configuration/AppletConfiguration.qml:353
msgid ""
"The settings of the current module have changed. Do you want to apply the "
"changes or discard them?"
msgstr ""
"Innstillingane i denne modulen er endra. Ønskjer du å bruka eller forkasta "
"endringane?"

#: contents/configuration/AppletConfiguration.qml:384
msgid "OK"
msgstr "OK"

#: contents/configuration/AppletConfiguration.qml:392
msgid "Apply"
msgstr "Bruk"

#: contents/configuration/AppletConfiguration.qml:398
msgid "Cancel"
msgstr "Avbryt"

#: contents/configuration/ConfigCategoryDelegate.qml:27
msgid "Open configuration page"
msgstr "Opnar oppsettsida"

#: contents/configuration/ConfigurationContainmentActions.qml:21
msgid "Left-Button"
msgstr "Venstreknappen"

#: contents/configuration/ConfigurationContainmentActions.qml:22
msgid "Right-Button"
msgstr "Høgreknappen"

#: contents/configuration/ConfigurationContainmentActions.qml:23
msgid "Middle-Button"
msgstr "Midtknappen"

#: contents/configuration/ConfigurationContainmentActions.qml:24
msgid "Back-Button"
msgstr "Tilbakeknapp"

#: contents/configuration/ConfigurationContainmentActions.qml:25
msgid "Forward-Button"
msgstr "Framknapp"

#: contents/configuration/ConfigurationContainmentActions.qml:27
msgid "Vertical-Scroll"
msgstr "Loddrett rulling"

#: contents/configuration/ConfigurationContainmentActions.qml:28
msgid "Horizontal-Scroll"
msgstr "Vassrett rulling"

#: contents/configuration/ConfigurationContainmentActions.qml:30
msgid "Shift"
msgstr "Shift"

#: contents/configuration/ConfigurationContainmentActions.qml:31
msgid "Ctrl"
msgstr "Ctrl"

#: contents/configuration/ConfigurationContainmentActions.qml:32
msgid "Alt"
msgstr "Alt"

#: contents/configuration/ConfigurationContainmentActions.qml:33
msgid "Meta"
msgstr "Meta"

#: contents/configuration/ConfigurationContainmentActions.qml:95
msgctxt "Concatenation sign for shortcuts, e.g. Ctrl+Shift"
msgid "+"
msgstr " + "

#: contents/configuration/ConfigurationContainmentActions.qml:167
msgctxt "@title"
msgid "About"
msgstr "Om"

#: contents/configuration/ConfigurationContainmentActions.qml:182
#: contents/configuration/MouseEventInputButton.qml:13
msgid "Add Action"
msgstr "Legg til handling"

#: contents/configuration/ConfigurationContainmentAppearance.qml:69
msgid "Layout changes have been restricted by the system administrator"
msgstr "Utformingsendringar er låste av systemadministratoren"

#: contents/configuration/ConfigurationContainmentAppearance.qml:84
msgid "Layout:"
msgstr "Utforming:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:98
msgid "Wallpaper type:"
msgstr "Type bakgrunn:"

#: contents/configuration/ConfigurationContainmentAppearance.qml:119
msgid "Get New Plugins…"
msgstr "Hent nye programtillegg …"

#: contents/configuration/ConfigurationContainmentAppearance.qml:189
msgid "Layout changes must be applied before other changes can be made"
msgstr "Du må ta i bruk utformingsendringane før du kan gjera andre endringar"

#: contents/configuration/ConfigurationContainmentAppearance.qml:193
msgid "Apply Now"
msgstr "Bruk no"

#: contents/configuration/ConfigurationShortcuts.qml:18
msgid "Shortcuts"
msgstr "Snarvegar"

#: contents/configuration/ConfigurationShortcuts.qml:30
msgid "This shortcut will activate the applet as though it had been clicked."
msgstr "Denne snøggtasten vil starta skjermelementet som om du trykte på det."

#: contents/configuration/ContainmentConfiguration.qml:27
msgid "Wallpaper"
msgstr "Bakgrunns­bilete"

#: contents/configuration/ContainmentConfiguration.qml:32
msgid "Mouse Actions"
msgstr "Muse­handlingar"

#: contents/configuration/MouseEventInputButton.qml:20
msgid "Input Here"
msgstr "Inndata her"

#: contents/configuration/PanelConfiguration.qml:91
msgid "Panel Settings"
msgstr "Panel­oppsett"

#: contents/configuration/PanelConfiguration.qml:98
msgid "Add Spacer"
msgstr "Legg til mellomrom"

#: contents/configuration/PanelConfiguration.qml:101
msgid "Add spacer widget to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:108
msgid "Add Widgets…"
msgstr "Legg til element …"

#: contents/configuration/PanelConfiguration.qml:111
msgid "Open the widget selector to drag and drop widgets to the panel"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:140
msgid "Position"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:145
#: contents/configuration/PanelConfiguration.qml:260
msgid "Top"
msgstr "Oppe"

#: contents/configuration/PanelConfiguration.qml:146
#: contents/configuration/PanelConfiguration.qml:262
msgid "Right"
msgstr "Til høgre"

#: contents/configuration/PanelConfiguration.qml:147
#: contents/configuration/PanelConfiguration.qml:260
msgid "Left"
msgstr "Til venstre"

#: contents/configuration/PanelConfiguration.qml:148
#: contents/configuration/PanelConfiguration.qml:262
msgid "Bottom"
msgstr "Nede"

#: contents/configuration/PanelConfiguration.qml:168
msgid "Set Position…"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:195
#, fuzzy
#| msgid "Alignment:"
msgid "Alignment"
msgstr "Plassering:"

#: contents/configuration/PanelConfiguration.qml:261
msgid "Center"
msgstr "I midten"

#: contents/configuration/PanelConfiguration.qml:284
msgid "Length"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:303
#, fuzzy
#| msgid "Panel height:"
msgid "Fill height"
msgstr "Panelhøgd:"

#: contents/configuration/PanelConfiguration.qml:303
#, fuzzy
#| msgid "Panel width:"
msgid "Fill width"
msgstr "Panelbreidd:"

#: contents/configuration/PanelConfiguration.qml:304
msgid "Fit content"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:305
msgid "Custom"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:335
msgid "Visibility"
msgstr "Synlegheit"

#: contents/configuration/PanelConfiguration.qml:348
#, fuzzy
#| msgid "Always Visible"
msgid "Always visible"
msgstr "Alltid synleg"

#: contents/configuration/PanelConfiguration.qml:349
#, fuzzy
#| msgid "Auto-Hide"
msgid "Auto hide"
msgstr "Gøym automatisk"

#: contents/configuration/PanelConfiguration.qml:350
msgid "Dodge windows"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:351
msgid "Windows Go Below"
msgstr "Vindauge kan liggja under"

#: contents/configuration/PanelConfiguration.qml:394
msgid "Opacity"
msgstr "Gjennomsikt"

#: contents/configuration/PanelConfiguration.qml:408
msgid "Adaptive"
msgstr "Adaptiv"

#: contents/configuration/PanelConfiguration.qml:409
msgid "Opaque"
msgstr "Ugjennomsiktig"

#: contents/configuration/PanelConfiguration.qml:410
msgid "Translucent"
msgstr "Gjennomsiktig"

#: contents/configuration/PanelConfiguration.qml:434
msgid "Style"
msgstr ""

#: contents/configuration/PanelConfiguration.qml:446
msgid "Floating"
msgstr "Flytande"

#: contents/configuration/PanelConfiguration.qml:520
#, fuzzy
#| msgid "Panel width:"
msgid "Panel Width:"
msgstr "Panelbreidd:"

#: contents/configuration/PanelConfiguration.qml:520
#, fuzzy
#| msgid "Right"
msgid "Height:"
msgstr "Til høgre"

#: contents/configuration/PanelConfiguration.qml:555
#, fuzzy
#| msgid "Focus Shortcut:"
msgid "Focus shortcut:"
msgstr "Fokussnarveg:"

#: contents/configuration/PanelConfiguration.qml:566
msgid "Press this keyboard shortcut to move focus to the Panel"
msgstr "Bruk snøggtasten til å flytta fokuset til panelet"

#: contents/configuration/PanelConfiguration.qml:582
#, fuzzy
#| msgid "Remove Panel"
msgctxt "@action:button Delete the panel"
msgid "Delete Panel"
msgstr "Fjern panelet"

#: contents/configuration/PanelConfiguration.qml:585
msgid "Remove this panel; this action is undo-able"
msgstr "Fjern panelet (kan ikkje angrast)"

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum height."
msgstr "Dra for å endra makshøgda."

#: contents/configuration/panelconfiguration/Ruler.qml:30
msgid "Drag to change maximum width."
msgstr "Dra for å endra maksbreidda."

#: contents/configuration/panelconfiguration/Ruler.qml:30
#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Double click to reset."
msgstr "Dobbeltklikka for å tilbakestilla."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum height."
msgstr "Dra for å endra minimumshøgda."

#: contents/configuration/panelconfiguration/Ruler.qml:31
msgid "Drag to change minimum width."
msgstr "Dra for å endra minimumsbreidda."

#: contents/configuration/panelconfiguration/Ruler.qml:78
msgid ""
"Drag to change position on this screen edge.\n"
"Double click to reset."
msgstr ""
"Dra for å endra plassering på denne skjermkanten.\n"
"Dobbeltklikk for å tilbakestilla."

#: contents/configuration/ShellContainmentConfiguration.qml:19
msgid "Panels and Desktops Management"
msgstr "Handsaming av panel og skrivebord"

#: contents/configuration/ShellContainmentConfiguration.qml:34
msgid ""
"You can drag Panels and Desktops around to move them to different screens."
msgstr "Du kan dra panela og skriveborda for å flytta dei mellom skjermane."

#: contents/configuration/ShellContainmentConfiguration.qml:44
msgid "Close"
msgstr "Lukk"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:179
msgid "Swap with Desktop on Screen %1"
msgstr "Byt med skrivebordet på skjerm %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:180
msgid "Move to Screen %1"
msgstr "Flytt til skjerm %1"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:194
msgid "Remove Desktop"
msgstr "Fjern skrivebord"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:195
msgid "Remove Panel"
msgstr "Fjern panelet"

#: contents/configuration/shellcontainmentconfiguration/Delegate.qml:268
msgid "%1 (primary)"
msgstr "%1 (hovudskjerm)"

#: contents/explorer/AppletAlternatives.qml:66
msgid "Alternative Widgets"
msgstr "Alternative element"

#: contents/explorer/AppletDelegate.qml:41
msgid "Unsupported Widget"
msgstr ""

#: contents/explorer/AppletDelegate.qml:188
msgid "Undo uninstall"
msgstr "Angra avinstallering"

#: contents/explorer/AppletDelegate.qml:189
msgid "Uninstall widget"
msgstr "Avinstaller element"

#: contents/explorer/Tooltip.qml:105
msgid "Author:"
msgstr "Laga av:"

#: contents/explorer/Tooltip.qml:115
msgid "Email:"
msgstr "E-postadresse:"

#: contents/explorer/Tooltip.qml:136
msgid "Uninstall"
msgstr "Avinstaller"

#: contents/explorer/WidgetExplorer.qml:118
#: contents/explorer/WidgetExplorer.qml:193
msgid "All Widgets"
msgstr "Alle element"

#: contents/explorer/WidgetExplorer.qml:144
msgid "Widgets"
msgstr "Skjermelement"

#: contents/explorer/WidgetExplorer.qml:153
msgid "Get New Widgets…"
msgstr "Hent nye element …"

#: contents/explorer/WidgetExplorer.qml:204
msgid "Categories"
msgstr "Kategoriar"

#: contents/explorer/WidgetExplorer.qml:286
msgid "No widgets matched the search terms"
msgstr "Ingen element passar til søkjeteksten"

#: contents/explorer/WidgetExplorer.qml:286
msgid "No widgets available"
msgstr "Ingen element er tilgjengelege"
